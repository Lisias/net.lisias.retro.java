# net.lisias.retro Java Services

This is the public repository for the net.lisias.retro.JAVA auxiliary services for the Distributed Web Services (aka "Confederation").


## Abbreviations and acronyms

Used in all documents on this project:

* WiP : Work In Progress
* RiP : Research In Progress ;-)


## What is it?

WiP

See https://www.py4j.org/download.html , https://applecommander.github.io .

Important for deploy: https://www.py4j.org/install.html

And https://github.com/robmcmullen/atrcopy (perhaps one day)


## Current tools:

* https://github.com/AppleCommander/AppleCommander


## Planned tools:

* Amiga
	+ https://github.com/weiju/adf-tools
* MS-DOS, Atari ST, MSX
	+ https://github.com/waldheinz/fat32-lib
* Commodore 64:
	+ https://github.com/bozimmerman/Emutil
	+ http://droid64.sourceforge.net
* Atari DOS (8-bit)
	+ https://pypi.python.org/pypi/atrcopy/6.2.0
* TI-99
	+ https://github.com/mizapf/tiimagetool
* Sinclair QL

## Links of interest

* https://github.com/Gregwar/fatcat
* https://github.com/macmade/fatdump
* https://github.com/SergiyKolesnikov/ddi2raw
* http://www.atarimania.com/faq-atari-400-800-xl-xe-what-file-formats-for-entire-disks-tapes-cartridges-are-there_81.html


## History

* 2018-0130: My fix was merged! :-)
	+ https://github.com/AppleCommander/AppleCommander/issues/18
* 2017-1130: I run into this problem today.
	+ https://github.com/AppleCommander/AppleCommander/issues/18
	+ Trying to solve it my self to keep working. If the AC guys allows me, I plan to rework it to submit a formal push request.


## Support

Drop a mail to ***support** *at* **lisias** *dot* **net***. I will be glad to help!

There's also a Forum on [Google+](https://plus.google.com/communities/114533428482860883026) and a site [here](http://retro.lisias.net/my/projects/network/).

Facebook? I don't do anything serious there. :-D

--
	2017/11
	Lisias
