package net.lisias.retro.java.util.python;

import py4j.GatewayServer;

public class Server {

	public static void main(String[] args) {
		GatewayServer gatewayServer = new GatewayServer();
		gatewayServer.start();
		System.out.println("Gateway Server Started");
	}
}
